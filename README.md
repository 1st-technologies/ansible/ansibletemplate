# Role Name

windows_poc: Demonstrate Windows management .

## Requirements

# Role Defaults

**ansible_port:** Specifies the WinRM port. Default: 5986  
**ansible_winrm_kinit_mode:** Specifies the kerberos initialization method. Manual requires one time "kinit" on the commandline prior to execution but is more compatible with Redhat Identity Manager. Default: manual  
**ansible_connection:** Specifies the connection method. Default: winrm  
**ansible_winrm_transport:** Specifies the WinRM transport and authentication method. Default: kerberos  
**ansible_winrm_server_cert_validation:** Specifies weather to ignore certificate errors (for self-signed certs). Default: ignore  
**ansible_winrm_scheme:** Specifies the transport method. Default: https  

# Dependencies

The target host must be configured to allow secure WinRM communication: See https://confluence.tradeweb.com/display/UH/How+to+Configure+a+Windows+Client+Server+for+Ansible+Remoting

The ansible host must have pywinrm(kerberos) and pywinrm(credssp) modules installed: See https://confluence.tradeweb.com/display/UH/How+to+Enable+Kerberos+for+Ansible+on+Windows

# Example Playbook

```
- hosts: pilot
  roles:
    - wmi_exporter
```

# License

View [license information](http://www.1st-technologies.com/license) for the software contained in this project.